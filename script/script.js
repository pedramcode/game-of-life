const DEATH = 0;
const ALIVE = 1;


class Game{
    constructor(_w, _h){
        this.w = _w;
        this.h = _h;
        this.matrix = [];
        for(var i = 0 ; i < _h ; i++){
            var tmp = [];
            for(var j = 0 ; j < _w ; j++){
                tmp.push(DEATH);
            }
            this.matrix.push(tmp);
        }
    }

    step(){
        for(var i = 0 ; i < this.h ; i++){
            for(var j = 0 ; j < this.w ; j++){
                var cells = [];
                if(i==0 && j==0){
                    cells.push(this.matrix[i+1][j]);
                    cells.push(this.matrix[i+1][j+1]);
                    cells.push(this.matrix[i][j+1]);
                }
                else if(i==0 && j==this.w-1){
                    cells.push(this.matrix[i+1][j]);
                    cells.push(this.matrix[i+1][j-1]);
                    cells.push(this.matrix[i][j-1]);
                }
                else if(i==this.h-1 && j==this.w-1){
                    cells.push(this.matrix[i-1][j]);
                    cells.push(this.matrix[i-1][j-1]);
                    cells.push(this.matrix[i][j-1]);
                }
                else if(i==this.h-1 && j==0){
                    cells.push(this.matrix[i-1][j]);
                    cells.push(this.matrix[i-1][j+1]);
                    cells.push(this.matrix[i][j+1]);
                }
                else if(i == 0){
                    cells.push(this.matrix[i][j-1]);
                    cells.push(this.matrix[i+1][j-1]);
                    cells.push(this.matrix[i+1][j]);
                    cells.push(this.matrix[i+1][j+1]);
                    cells.push(this.matrix[i][j+1]);
                }
                else if(j == this.w-1){
                    cells.push(this.matrix[i-1][j]);
                    cells.push(this.matrix[i-1][j-1]);
                    cells.push(this.matrix[i][j-1]);
                    cells.push(this.matrix[i+1][j-1]);
                    cells.push(this.matrix[i+1][j]);
                }
                else if(i == this.h-1){
                    cells.push(this.matrix[i][j-1]);
                    cells.push(this.matrix[i-1][j-1]);
                    cells.push(this.matrix[i-1][j]);
                    cells.push(this.matrix[i-1][j+1]);
                    cells.push(this.matrix[i][j+1]);
                }
                else if(j == 0){
                    cells.push(this.matrix[i-1][j]);
                    cells.push(this.matrix[i-1][j+1]);
                    cells.push(this.matrix[i][j+1]);
                    cells.push(this.matrix[i+1][j]);
                    cells.push(this.matrix[i+1][j+1]);
                }else{
                    cells.push(this.matrix[i-1][j-1]);
                    cells.push(this.matrix[i-1][j]);
                    cells.push(this.matrix[i-1][j+1]);
                    cells.push(this.matrix[i][j-1]);
                    cells.push(this.matrix[i][j+1]);
                    cells.push(this.matrix[i+1][j-1]);
                    cells.push(this.matrix[i+1][j]);
                    cells.push(this.matrix[i+1][j+1]);
                }
                var alive = 0;
                cells.forEach(cell => {
                    if(cell == ALIVE){
                        alive++;
                    }
                });
                var current = this.matrix[i][j];
                if(current == ALIVE && alive < 2){
                    this.matrix[i][j] = DEATH;
                }else if(current == ALIVE && (alive == 2 || alive == 3)){
                    // next
                }else if(current == ALIVE && alive > 3){
                    this.matrix[i][j] = DEATH;
                }else if(current == DEATH && alive == 3){
                    this.matrix[i][j] = ALIVE;
                }
            }
        }
    }

    render(){
        push();
        noStroke();
        for(var i = 0 ; i < this.h ; i++){
            for(var j = 0 ; j < this.w ; j++){
                var cell = this.matrix[i][j];
                if(cell == ALIVE){
                    fill(color(255,255,255));
                }else if(cell==DEATH){
                    fill(color(0,0,0));
                }
                rect(j * width / this.w, i* height / this.h, width / this.w,height / this.h);
            }
        }
        noFill();
        stroke(color(255,255,255,100));
        strokeWeight(1);
        for(var i = 0 ; i < width ; i+= width/this.w){
            line(i, 0, i, height);
        }
        for(var i = 0 ; i < height ; i+= height/this.h){
            line(0, i, width, i);
        }
        pop();
    }
}


var game;

function setup(){
    createCanvas(500,500);
    game = new Game(50,50);
    game.matrix[10][10] = ALIVE;
    game.matrix[10][11] = ALIVE;
    game.matrix[10][12] = ALIVE;
    game.matrix[11][9] = ALIVE;
    game.matrix[11][10] = ALIVE;
    game.matrix[11][11] = ALIVE;
}

function draw(){
    frameRate(20);
    background(10);
    game.step();
    game.render();
}